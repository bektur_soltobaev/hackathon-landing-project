from django.contrib import admin
from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Hackatjon API')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('teams/', include('apps.pies.urls')),
    path('participants/', include('apps.participants.urls')),
    path('', include('apps.posts.urls')),
    path('', include('frontend.urls')),
    path('swagger/', schema_view)
]
