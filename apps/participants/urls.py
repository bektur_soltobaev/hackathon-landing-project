from django.urls import path
from .views import ParticipantViewSet

urlpatterns = [
    path('', ParticipantViewSet.as_view({'get': 'list', 'post': 'create'}), name='participants_list_url'),
    path('<int:id>/', ParticipantViewSet.as_view({'get': 'retrieve', 'put': 'update',
                                                  'delete': 'destroy'}), name='participants_detail_url')
]
