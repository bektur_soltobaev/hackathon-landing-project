from django.db import models
from django.urls import reverse

ROLE_CHOICES = {
    (1, 'IT специалист'),
    (2, "Дизайнер"),
    (3, "Гражданский активист"),
    (4, "Эксперт по парламентаризму"),
    (5, "Преподаватель по гражданскому образованию")
}


class Participant(models.Model):
    first_name = models.CharField(max_length=128, verbose_name='Имя')
    last_name = models.CharField(max_length=128, verbose_name="Фамилия")
    role = models.IntegerField(choices=ROLE_CHOICES, verbose_name="Роль")

    def __str__(self):
        return self.first_name

    def get_absolute_url(self):
        return reverse('participant_detail_url', kwargs={'pk': self.pk})
