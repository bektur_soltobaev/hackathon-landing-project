from django.db import models
from django.urls import reverse


class Team(models.Model):
    name = models.CharField(max_length=128, unique=True)
    developer = models.CharField(max_length=128, blank=True)
    designer = models.CharField(max_length=128, blank=True)
    civil_activist = models.CharField(max_length=128, blank=True)
    expert = models.CharField(max_length=128, blank=True)
    teacher = models.CharField(max_length=128, blank=True)
    project_summary = models.TextField(default='Описание моего проекта')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('team_detail_url', kwargs={'pk': self.pk})
