from django.apps import AppConfig


class PiesConfig(AppConfig):
    name = 'apps.pies'
