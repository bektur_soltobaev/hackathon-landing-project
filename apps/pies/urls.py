from django.urls import path
from .views import TeamViewSet

urlpatterns = [
    path('', TeamViewSet.as_view({'get': 'list', 'post': 'create'}), name='teams_list_url'),
    path('<int:id>/', TeamViewSet.as_view({'get': 'retrieve', 'put': 'update',
                                           'delete': 'destroy'}), name='team_detail_url')
]
