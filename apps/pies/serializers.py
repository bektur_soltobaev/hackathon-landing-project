from rest_framework import serializers
from .models import Team


class TeamSerializer(serializers.ModelSerializer):
    developer = serializers.CharField(required=False)
    designer = serializers.CharField(required=False)
    expert = serializers.CharField(required=False)
    civil_activist = serializers.CharField(required=False)

    class Meta:
        model = Team
        fields = ('name', 'developer', 'designer', 'expert', 'civil_activist', 'teacher', 'project_summary')
