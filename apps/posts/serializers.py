from rest_framework import serializers
from .models import Post


class PostSerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField(format="%d-%m-%Y %H:%M", required=False)

    class Meta:
        model = Post
        fields = ('title', 'summary', 'image', 'date')
