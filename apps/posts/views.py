from django.shortcuts import render
from .models import Post
from rest_framework import viewsets
from .serializers import PostSerializer

# def index_view(request):
#     posts = Post.objects.all()
#     return render(request, 'posts/index.html', context={'posts': posts})


class PostViewSet(viewsets.ModelViewSet):
    serializer_class = PostSerializer
    lookup_field = 'id'
    queryset = Post.objects.all()