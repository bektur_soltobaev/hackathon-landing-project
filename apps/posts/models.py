from django.db import models


class Post(models.Model):
    title = models.CharField(max_length=128)
    summary = models.TextField(blank=True)
    image = models.ImageField()
    date = models.DateTimeField(blank=True)

    def __str__(self):
        return self.title
